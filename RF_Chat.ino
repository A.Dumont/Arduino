/*
 * Simple programm for chatting between Arduinos equipped with the eRIC Arduino Shield
 * 
 * By : Antoine Dumont
 * Date : June 4th, 2018
 * 
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <antoine.dumont@etu.uca.fr> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. Antoine Dumont
 * ----------------------------------------------------------------------------
 */

#include <SoftwareSerial.h>

SoftwareSerial eRIC(2,3); //Creation of a serial communication

String sentData = ""; //String to send

void printWelcome(); //Display the banner

void setup() {
  Serial.begin(19200); //Open serial communication
  while(!Serial); //Wait for the port to open
  
  eRIC.begin(19200); //Open serial communication with eRIC module with it's default baud rate
  if(!eRIC) //Check whether the serial port is opened
    Serial.println("Problem initializing communication");
  else
    printWelcome();
}

void loop() {
  /* Reception */
  if(eRIC.available()){ //If there is incoming data on eRIC port
    Serial.print("Received : ");
    while(eRIC.available()) //Display all of them on serial monitor
      Serial.print((char)eRIC.read());
  }

  /* Sending */
  if(Serial.available()){ //If there is incoming data on Serial port
    Serial.print("Sent : ");
    while(Serial.available())
      sentData+=(char)Serial.read(); //Concatenate all the typed-in bytes
    eRIC.println(sentData); //Send the string on eRIC port
    Serial.println(sentData);
    sentData = ""; //Empty the string
  }  
  
  delay(100);
}

void printWelcome(){
 Serial.println("\tWelcome to RF Chat !");
 Serial.println("\t   ,              ,");
 Serial.println("\t /   ,         ,   \\");
 Serial.println("\t(   (   ( O )   )   )");
 Serial.println("\t \\   `   /_\\   ‘   /");
 Serial.println("\t   ‘    /___\\    ‘");
 Serial.println("\t       /     \\");
 Serial.println();
}

